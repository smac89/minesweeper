package minesweeper

import kotlin.math.min
import kotlin.random.Random

class MineSweeperBoard(val rows: Int, val cols: Int) {
    private val fieldModel by lazy {
        MutableList(rows) { MutableList(cols) { Cell() } }
    }

    private val presumedMines: MutableSet<IndexedCell> by lazy(::mutableSetOf)

    private var _minesCount = 0
    private var _minesFlagged = 0
    private var _exploredCount = 0
    private var _mineHit = false

    private val cellsCount
        get() = rows * cols

    val sweepVictory
        get() = !_mineHit ||
                _minesFlagged == _minesCount ||
                _exploredCount + _minesCount == cellsCount

    private val unexploredState
        get() = _exploredCount == 0

    /**
     * Sets the number of mines contained on the board
     * @param minesCount Int The number of mines
     */
    fun fillWithMines(minesCount: Int) {
        require(minesCount > 0)
        this._minesCount = min(minesCount, rows * cols)
    }

    /**
     * Flags a cell if it is not yet explored
     * @param row Int Row of cell
     * @param col Int Column of cell
     */
    fun flagCell(row: Int, col: Int) {
        val cell = fieldModel[row][col]

        if (cell.explored) {
            return
        }

        fieldModel[row][col] = cell.copy(flagged = !cell.flagged)

        when {
            unexploredState -> {
                if (fieldModel[row][col].flagged) {
                    presumedMines.add(row to col)
                } else {
                    presumedMines.remove(row to col)
                }
            }
            else -> {
                if (cell.type is Mine) {
                    if (fieldModel[row][col].flagged) {
                        _minesFlagged++
                    } else {
                        _minesFlagged--
                    }
                }
            }
        }
    }

    /**
     * Explores a given cell uncovering all free cells around it
     * @param row Int Row of the cell
     * @param col Int Column of the cell
     */
    fun exploreCell(row: Int, col: Int) {
        val origin = row to col

        if (unexploredState) {
            initializeMineField(origin)
            initializeMineCount()
        }

        if (fieldModel[row][col].explored) {
            return
        }

        _mineHit = fieldModel[row][col].type is Mine

        if (_mineHit) {
            seppukuMode()
        } else {
            _exploredCount += autoExplorer(origin)
        }
    }

    /**
     * Explore only mines.
     * What a sad state :(
     */
    private fun seppukuMode() {
        (0 until rows).flatMap { row ->
            (0 until cols).map { row to it }
        }.filter { cellForIndexCell(it).type is Mine }.onEach { autoExplorer(it, true) }
    }

    /**
     * Initialize the mine field by generating mines in areas not conntected
     * to origin
     * @param origin Pair<Int, Int> The cell which we want to be free of mines
     */
    private fun initializeMineField(origin: IndexedCell) {
        val (origRow, origCol) = origin
        val neighbors = getNeighbours(origin).toSet()

        var usedMines = 0
        while (usedMines < _minesCount) {
            val row = Random.nextInt(rows)
            val col = Random.nextInt(cols)
            val cell = fieldModel[row][col]

            if ((row != origRow || col != origCol) && (row to col !in neighbors) && cell.type !is Mine) {
                fieldModel[row][col] = cell.copy(type = Mine)
                usedMines++
            }
        }

        for ((row, col) in presumedMines) {
            fieldModel[row][col] = fieldModel[row][col].copy(flagged = true)
            if (fieldModel[row][col].type is Mine) {
                _minesFlagged++
            }
        }
        presumedMines.clear()
    }

    /**
     * Populate the field with the counts of neighbouring mines
     */
    private fun initializeMineCount() {
        fieldModel.withIndex().onEach { (rowIndex, fieldRow) ->
            for (cellIndex in 0 until fieldRow.size) {
                val cell = fieldRow[cellIndex]
                if (cell.type is Mine) {
                    continue
                }
                val neighboringMines = getNeighbours(rowIndex to cellIndex).filter {
                    cellForIndexCell(it).type is Mine
                }

                if (neighboringMines.isNotEmpty()) {
                    fieldRow[cellIndex] = cell.copy(type = MineCount('0' + neighboringMines.size))
                }
            }
        }
    }

    /**
     * Automatically explore all free cells around origin
     * @param origin Pair<Int, Int> The place to start exploring from
     * @param suicideMode Boolean Whether or not to just explore mines
     * @return Int number of cells explored
     */
    private fun autoExplorer(origin: IndexedCell, suicideMode: Boolean = false): Int {
        val freeCells: MutableList<IndexedCell> = mutableListOf(origin)
        var numExplored = 0

        while (freeCells.isNotEmpty()) {
            val (row, col) = freeCells.removeAt(freeCells.size - 1)
            val cell = fieldModel[row][col]

            if (cell.explored) {
                continue
            }

            fieldModel[row][col] = cell.copy(explored = true)
            numExplored++

            var neighbors = getNeighbours(row to col)
            if (suicideMode) {
                neighbors = neighbors.filter { cellForIndexCell(it).type is Mine }
            } else if (neighbors.any { cellForIndexCell(it).type is Mine }) {
                neighbors = listOf()
            }

            freeCells.addAll(neighbors)
        }

        return numExplored
    }

    /**
     * Get the neighbours of a given cell
     * @param cell Pair<Int, Int> The row and column of the cell
     * @return List<Pair<Int, Int>> The neighbours of the cell
     */
    private fun getNeighbours(cell: IndexedCell): List<IndexedCell> {
        val (row, col) = cell

        return arrayOf(
            (row - 1) to (col - 1),
            (row - 1) to (col),
            (row - 1) to (col + 1),
            (row + 1) to (col - 1),
            (row + 1) to (col),
            (row + 1) to (col + 1),
            (row) to (col - 1),
            (row) to (col + 1)).filter {
            it.first in 0 until rows && it.second in 0 until cols
        }
    }

    private fun cellForIndexCell(cell: IndexedCell): Cell = fieldModel[cell.first][cell.second]

    override fun toString(): String {
        val topRow = (1..cols).joinToString("", " |", "|")
        val board = fieldModel.withIndex().joinToString("\n") { (index, value) ->
            "${index + 1}|${value.joinToString("")}|"
        }
        val divider = generateSequence { '-' }.take(cols).joinToString("", "-|", "|")
        return "$topRow\n$divider\n$board\n$divider"
    }
}

internal typealias IndexedCell = Pair<Int, Int>
