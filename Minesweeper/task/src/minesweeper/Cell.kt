package minesweeper

data class Cell(val type: CellType = Free, val flagged: Boolean = false, val explored: Boolean = false) {
    private val value get() = type.value

    override fun toString(): String {
        return if (!explored && flagged) "*" else if (!explored) "." else value.toString()
    }
}

sealed class CellType(val value: Char)
object Free : CellType('/')
object Mine : CellType('X')
class MineCount(value: Char) : CellType(value) {
    init {
        require(value in '1'..'8') {
            "Invalid mine count: $value should be in range '1'..'8'"
        }
    }
}
