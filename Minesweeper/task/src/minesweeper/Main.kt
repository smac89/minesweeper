package minesweeper

import java.util.*

const val DIM = 9

fun main() {
    val mines = prompt("How many mines do you want on the field?", String::toInt)
    val mineSweeperBoard = MineSweeperBoard(DIM, DIM).apply { fillWithMines(mines) }
    println("\n$mineSweeperBoard")
    game(mineSweeperBoard)
}

fun game(board: MineSweeperBoard) {
    loop@ do {
        val (x, y, cmd) = prompt("Set/unset mine marks or claim a cell as free:") {
            it.split(" ").run {
                Triple(elementAtOrNull(0)?.toInt() ?: 0,
                    elementAtOrNull(1)?.toInt() ?: 0,
                    elementAtOrNull(2) ?: "")
            }
        }
        if (x !in 1..board.cols || y !in 1..board.rows) {
            println("Invalid coordinates. Try again...")
            continue
        }
        when (cmd) {
            "mine" -> board.flagCell(y - 1, x - 1)
            "free" -> board.exploreCell(y - 1, x - 1)
            else -> {
                println("Invalid command. Expected 'free' or 'mine'")
                continue@loop
            }
        }
        println("\n$board")
    } while (!board.sweepVictory)

    if (!board.sweepVictory) {
        println("You stepped on a mine and failed!")
    } else {
        println("Congratulations! You found all the mines!")
    }
}

@Suppress("ClassName")
object prompt {
    private val reader: Scanner = Scanner(System.`in`)
    operator fun <T> invoke(msg: String, cvt: (String) -> T): T {
        print("$msg > ")
        return cvt(reader.nextLine())
    }
}
